package com.example.fragmentsapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import com.example.fragmentsapp.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private lateinit var bottomMenuEnabler: BottomMenuEnabler

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BottomMenuEnabler) {
            bottomMenuEnabler = context
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        val view = binding.root
        bottomMenuEnabler.disableMenu()
        binding.buttonLogin.setOnClickListener {
            bottomMenuEnabler.enableMenu()
            Navigation.findNavController(view).navigate(R.id.action_login_to_home)
        }
        binding.buttonRegistrate.setOnClickListener {
            Navigation.findNavController(view).navigate(R.id.action_login_to_registration)
        }
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}