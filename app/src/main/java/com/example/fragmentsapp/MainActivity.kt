package com.example.fragmentsapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.fragment.app.commit
import com.example.fragmentsapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), BottomMenuEnabler {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.bottomView.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.itemHome -> {
                    supportFragmentManager.commit {
                        setReorderingAllowed(true)
                        replace(binding.fragmentContainerView.id, HomeFragment())
                    }
                }
                R.id.itemWeb -> {
                    supportFragmentManager.commit {
                        setReorderingAllowed(true)
                        replace(binding.fragmentContainerView.id, WebFragment())
                    }
                }
            }
            true
        }
    }

    override fun enableMenu() {
        binding.bottomView.visibility = View.VISIBLE
    }

    override fun disableMenu() {
        binding.bottomView.visibility = View.INVISIBLE
    }
}