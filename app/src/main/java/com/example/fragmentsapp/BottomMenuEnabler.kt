package com.example.fragmentsapp

interface BottomMenuEnabler {
    fun enableMenu() {}
    fun disableMenu() {}
}